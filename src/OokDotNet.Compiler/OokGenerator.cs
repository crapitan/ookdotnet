using System;

namespace OokDotNet.Compiler
{
    public class OokGenerator : BrainfuckGenerator
    {
        protected override void Usage ()
        {
            Console.WriteLine (
                "Usage: ook [options] source-file\n" +
                "Options:\n" +
                "  -ook!         Display this information\n" + 
                "  -ook?         Generate debugging information\n" + 
                "  -ook <file>   Specifies output file\n\n" +
                "For more information on Ook programming language, please see:\n" +
                "   http://esolangs.org/wiki/ook!");
        }
		
        protected override void Logo ()
        {
            Console.WriteLine(
                "OokDotnet is based upong Ook# by Lawrence Pit (Lawrence.Pit@gmail.com)" +
                "Original Source can be found at https://github.com/lawrencepit/Esoteric");
        }
    }
}