//
// ook.cs: The Ook# compiler v1.0
//
// Author:
//  Original Author: Lawrence Pit (Lawrence.Pit@gmail.com)
//  Rework: Klas Broberg (klas.broberg@gmail.com)

using System.IO;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;

namespace OokDotNet.Compiler
{
    internal class OokStream : FileStream
	{	
		private string source;
		private int index = 0;
		private int len = 0;
		
		public string OokMatch (Match match)
		{
			string str1 = match.Groups [1].Captures [0].Value;
			string str2 = match.Groups [2].Captures [0].Value;
			
			switch (str1)
			{
			    case ".":
			        switch (str2)
			        {
			            case "?":
			                return ">";
			            case ".":
			                return "+";
			            case "!":
			                return ",";
			        }
			        break;
			    case "?":
			        switch (str2)
			        {
			            case ".":
			                return "<";
			            case "!":
			                return "]";
			        }
			        break;
			    case "!":
			        switch (str2)
			        {
			            case "!":
			                return "-";
			            case ".":
			                return ".";
			            case "?":
			                return "[";
			        }
			        break;
			}
			return string.Empty;
		}
		
		public OokStream (string path) : base (path, FileMode.Open, FileAccess.Read, FileShare.Read)
		{
			var b = new StringBuilder ((int) base.Length);
			int c = 0;

		    while ((c = base.ReadByte()) != -1)
		    {
		        b.Append ((char) c);
		    }

			source = b.ToString ();
			source = Regex.Replace (source, @"[+-,\[\]<>]", "");
			var r = new Regex (@"Ook([?!\.])\s+Ook([?!\.])");
			source = r.Replace (source, this.OokMatch);
			source = source.Replace (" ", string.Empty);
			source = source.Replace ("\r", string.Empty);
			source = source.Replace ("\n", string.Empty);
			len = source.Length;
		}
		
		public override int ReadByte ()
		{
			return index == len ? -1 : source [index++];	
		}
	}
}
