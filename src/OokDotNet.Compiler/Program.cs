﻿using System;
using System.IO;

namespace OokDotNet.Compiler
{
    /// <summary>
    /// Ook! 
    /// I'm going totaly librarian on you
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            var ook = new OokGenerator
            {
                prefixErr = "Oooook!   (English translation: ",
                postfixErr = ")",
                openingToken = "Ook! Ook?",
                closingToken = "Ook? Ook!"
            };

            if (!ook.Parse(args))
            {
                return;
            }

            try
            {
                var ookStream = new OokStream(ook.Path);
                
                var assembly = ook.Compile(ookStream);

                ookStream.Close();
                ook.Save(assembly);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("{0}File {1} not found!{2}", ook.prefixErr, ook.Path, ook.postfixErr);
            }
        }
    }
}
