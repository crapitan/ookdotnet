//
// bf.cs: The Brainf*ck compiler v1.0
//
// Author:
//  Original Author: Lawrence Pit (Lawrence.Pit@gmail.com)
//  Rework: Klas Broberg (klas.broberg@gmail.com)

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;

namespace OokDotNet.Compiler
{
	public class BrainfuckGenerator 
	{
	    public string prefixErr = "F***! ";
	    public string postfixErr = string.Empty;
	    public string openingToken = "[ token";
	    public string closingToken = "] token";

	    public BrainfuckGenerator()
	    {
	        UnixCompatible = true;
	        Debug = false;
	        OutputFileName = null;
	        Path = null;
	    }

	    protected virtual void Usage ()
		{
			Console.WriteLine (
				"Usage: bf [options] source-file\n" +
				"Options:\n" +
				"  -h, --help    Display this information\n" + 
				"  -debug        Generate debugging information\n" + 
				"  -o <file>     Specifies output file\n\n" +
				"For more information on Brainf*ck, please see:\n" +
				"   http://bluesorcerer.net\n");
		}
		
		protected virtual void Logo ()
		{
			Console.WriteLine (
                "Modified by Klas Broberg" +
                "Based on \n" +
				"BlueSorcerer (R) Brainf*ck .NET Compiler version 1.0\n" +
				"Copyright (C) BlueSorcerer 2002. All rights reserved.\n"); 

		}

	    public string Path { get; private set; }

	    protected string OutputFileName { get; private set; }

	    protected bool Debug { get; private set; }

	    protected bool UnixCompatible { get; private set; }

	    public bool Parse (string[] args)
		{	
			Logo ();
			
			int argc = args.Length;		
			if (argc == 0) {
				Console.WriteLine (prefixErr + "No input files!" + postfixErr);
				return false;
			}
			for (int i = 0; i < argc; i++)
            {
				string arg = args [i];
				
				if (arg.StartsWith ("-")) {
					switch (arg) {
					case "-h":					
					case "--help":
					case "-ook!":
						Usage ();
						return false;	
						
					case "-debug": 
					case "-ook?":
						Debug = true;
						continue;

					case "-o": 
					case "-ook":
						if (++i >= argc) {
							Usage ();
							return false;
						}
						OutputFileName = args [i];
						continue;
					default:
						Console.WriteLine (prefixErr + "Unknown option: " + arg + postfixErr);
						return false;
					}
				} else if ((i + 1) != argc) {
					Console.WriteLine (prefixErr + "Unknown option: " + arg + postfixErr);
					return false;
				} else {
					this.Path = arg;
				}
			}	

			if (this.Path == null) {
				Console.WriteLine (prefixErr + "No input files!" + postfixErr);
				return false;
			}
			return true;
		}

	    public AssemblyBuilder Compile (Stream stream)
		{		
			string dirName = ".";
			string fileName = this.Path;
			int pos = this.Path.LastIndexOfAny (new char[] {'/', '\\'});
			
            if (pos != -1) {
				dirName = this.Path.Substring (0, pos);
				fileName = this.Path.Substring (pos + 1);
			}
			
            string baseName = fileName;
			
            pos = fileName.LastIndexOf ('.');
	        
            if (pos != -1)
	        {
	            baseName = fileName.Substring (0, pos);
	        }

	        if (OutputFileName == null)
	        {
	            OutputFileName = baseName + ".exe";
	        }
			
			AppDomain curDomain = AppDomain.CurrentDomain;
			var an = new AssemblyName { Name = baseName };
	        AssemblyBuilder assembly = curDomain.DefineDynamicAssembly (an, AssemblyBuilderAccess.RunAndSave, dirName);
			ModuleBuilder module = assembly.DefineDynamicModule (fileName, OutputFileName, Debug);
            MethodBuilder method = module.DefineGlobalMethod ("ook", MethodAttributes.Public | MethodAttributes.Static, typeof (void), null);

			assembly.SetEntryPoint (method, PEFileKinds.ConsoleApplication);
				
			ILGenerator mainIL = method.GetILGenerator ();

			LocalBuilder ookPtr = mainIL.DeclareLocal (typeof (int));
			mainIL.Emit (OpCodes.Ldc_I4_0);
			mainIL.Emit (OpCodes.Stloc, ookPtr);

			LocalBuilder ookArray = mainIL.DeclareLocal (typeof (int []));
			mainIL.Emit (OpCodes.Ldc_I4, 30000);
			mainIL.Emit (OpCodes.Newarr, typeof (int));
			mainIL.Emit (OpCodes.Stloc, ookArray);
			
  		        MethodInfo writeMethod = typeof(Console).GetMethod("Write", new Type [] {typeof (Char)});
  		        MethodInfo readMethod = typeof(Console).GetMethod("Read", new Type [] {});

			Stack labels = new Stack ();

			while (true) {
				int c = stream.ReadByte ();
				if (c == -1)
					break;
				switch (c) {
				/* + */ case 43 :
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldelem_I4);
					mainIL.Emit (OpCodes.Ldc_I4_1);
					mainIL.Emit (OpCodes.Add);
					mainIL.Emit (OpCodes.Stelem_I4);
					break;
				/* , */ case 44 : 
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Call, readMethod);
					if (UnixCompatible) {
						// Some bf programs assume -1 is read upon EOF 
						// (the original assumes 0, as does this compiler)					
						Label endLabel = mainIL.DefineLabel ();
						Label notEOF = mainIL.DefineLabel ();
						Label notLF = mainIL.DefineLabel ();
						mainIL.Emit (OpCodes.Dup);
						mainIL.Emit (OpCodes.Ldc_I4_1);     // if -1
						mainIL.Emit (OpCodes.Add);					
						mainIL.Emit (OpCodes.Brtrue, notEOF);
						mainIL.Emit (OpCodes.Ldc_I4_1);     // then correct to 0
						mainIL.Emit (OpCodes.Add);
						mainIL.Emit (OpCodes.Br, endLabel); 
						mainIL.MarkLabel (notEOF); 
						mainIL.Emit (OpCodes.Dup);
						mainIL.Emit (OpCodes.Ldc_I4, 13);   // if LF
						mainIL.Emit (OpCodes.Sub);
						mainIL.Emit (OpCodes.Brtrue, notLF);
						mainIL.Emit (OpCodes.Ldc_I4_3);     // then switch to CR
						mainIL.Emit (OpCodes.Sub);
						mainIL.Emit (OpCodes.Br, endLabel); 
						mainIL.MarkLabel (notLF);
						mainIL.Emit (OpCodes.Dup);
						mainIL.Emit (OpCodes.Ldc_I4, 10);   // if CR
						mainIL.Emit (OpCodes.Sub);
						mainIL.Emit (OpCodes.Brtrue, endLabel);
						mainIL.Emit (OpCodes.Ldc_I4_3);     // then switch to LF
						mainIL.Emit (OpCodes.Add);
						mainIL.MarkLabel (endLabel);
					}
					mainIL.Emit (OpCodes.Stelem_I4);
					break;
				/* - */ case 45 :
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldelem_I4);
					Label notTooNegative = mainIL.DefineLabel ();
					mainIL.Emit (OpCodes.Dup);
					mainIL.Emit (OpCodes.Ldc_I4, 256);
					mainIL.Emit (OpCodes.Add);
					mainIL.Emit (OpCodes.Brtrue, notTooNegative);
					mainIL.Emit (OpCodes.Pop);	// wrap from -256 to 255
					mainIL.Emit (OpCodes.Ldc_I4, 256);
					mainIL.MarkLabel (notTooNegative);
					mainIL.Emit (OpCodes.Ldc_I4_1);
					mainIL.Emit (OpCodes.Sub);
					mainIL.Emit (OpCodes.Stelem_I4);					
					break;
				/* . */ case 46 :
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldelem_I4);
					mainIL.Emit (OpCodes.Call, writeMethod);
					break;
				/* < */ case 60 :
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldc_I4_1);
					mainIL.Emit (OpCodes.Sub);
					mainIL.Emit (OpCodes.Stloc, ookPtr);
					break;
				/* > */ case 62 :
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldc_I4_1);
					mainIL.Emit (OpCodes.Add);
					mainIL.Emit (OpCodes.Stloc, ookPtr);
					break;
				/* [ */ case 91 :
					Label startWhile = mainIL.DefineLabel ();
					Label endWhile = mainIL.DefineLabel ();					
					labels.Push (endWhile);					
					labels.Push (startWhile);
					mainIL.MarkLabel (startWhile);
					mainIL.Emit (OpCodes.Ldloc, ookArray);
					mainIL.Emit (OpCodes.Ldloc, ookPtr);
					mainIL.Emit (OpCodes.Ldelem_I4);
					mainIL.Emit (OpCodes.Brfalse, endWhile);					
					break;
				/* ] */ case 93 :
					if (labels.Count == 0) {
						Console.WriteLine (prefixErr + "Syntax error: closing " + closingToken + " appeared before an opening " + openingToken + postfixErr);
						return null;
					}
					mainIL.Emit (OpCodes.Br, (Label) labels.Pop ());
					mainIL.MarkLabel ((Label) labels.Pop ());
					break;
				}				
			}
			
			if (labels.Count > 0) {
				Console.WriteLine (prefixErr + "Syntax error: missing " + closingToken + postfixErr);
				return null;
			}
						
		        mainIL.Emit(OpCodes.Ret);
				
			module.CreateGlobalFunctions ();
			
			return assembly;
		}

	    public void Save (AssemblyBuilder assembly)
		{			
			if (assembly == null)
				return;
			assembly.Save (OutputFileName);			
		}
	}
}
